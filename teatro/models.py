from django.db import models

# Create your models here.

class Arte(models.Model):
   nombre = models.CharField(max_length=200)
   class Meta:
      ordering = ["nombre"]


class Artista(models.Model):
   nombre = models.CharField(max_length=200)
   nombre_A = models.CharField(max_length=200)
   edad = models.IntegerField(blank=True, null=True)
   arte = models.ForeignKey(Arte, on_delete=models.CASCADE, related_name='artistas', null=True)   # arte.artistas.all() se va a poder obtener todas las marcas

   class Meta:
      ordering = ["nombre"]