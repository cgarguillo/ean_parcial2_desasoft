from django.shortcuts import render,redirect
from teatro.models import Arte, Artista
from teatro.helpers.cargas import cargar_arte_basic, cargar_artista_basic, modificar_artista_basic
from django.db import IntegrityError

# Create your views here.

def cargar_arte(request):
    lista_arte = Arte.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            cargar_arte_basic(nombre)
        except Exception as err:
            return render(request, 'arte_carga.html', {'lista_arte': lista_arte,'error': str(err)})
    return render(request, 'arte_carga.html', {'lista_arte': lista_arte})


def cargar_artista(request):
    lista_artista = Artista.objects.all()
    lista_arte = Arte.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            nombre_A = request.POST.get('nombre_A') #reemplazamos contenido
            edad = request.POST.get('edad')
            arte_id = request.POST.get('arte_id')
            cargar_artista_basic(nombre,nombre_A,edad,arte_id)


        except IntegrityError as otroErr:
            err = 'Error en Sqlite3'
            return render(request, 'lista_artista.html',{'lista_artista': lista_artista, 'lista_arte': lista_arte, 'error': str(otroErr)})
        except Exception as err:
            return render(request, 'lista_artista.html', {'lista_artista': lista_artista, 'lista_arte': lista_arte,'error':str(err)})
    return render(request, 'lista_artista.html', {'lista_artista': lista_artista, 'lista_arte': lista_arte})


def modificar_artista(request, id_artista):  #Use la base de modifar para ELIMINAR!!!
    artista = Artista.objects.get(id=id_artista)
    lista_arte = Arte.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            nombre_A = request.POST.get('nombre_A') #reemplazamos contenido
            edad = request.POST.get('edad')
            arte_id = request.POST.get('arte_id')
            modificar_artista_basic(artista,nombre,nombre_A,edad,arte_id)

        except IntegrityError as otroErr:
            err = 'Error en Sqlite3'
            return render(request, 'modificar_artista.html',{'artista': artista, 'lista_arte': lista_arte, 'error': str(otroErr)})
        except Exception as err:
            return render(request, 'modificar_artista.html', {'artista': artista, 'lista_arte': lista_arte,'error':str(err)})

        return redirect('/colon/cargar_artista')

    return render(request, 'modificar_artista.html', {'artista': artista, 'lista_arte': lista_arte})